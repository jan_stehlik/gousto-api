# REST API Test #
## General
Application is written in PHP using Laravel 5.6 framework. 
Application doesn't communicate with database.
All endpoints return JSON response.
Primary data store used is csv file `recipe-data.csv` 
in public folder.

## Requirements
* PHP 7.1.3 or higher
* composer

## Usage
To install and run the app navigate to app directory and 
run make.
```
cd /path/to/gousto-api
make
```
Project now runs on localhost on port `8000`. To access
endpoint run `127.0.0.1:8000/<endpoint_uri>`.

By default app looks for `recipe-data.csv` in `/public`
folder for a primary data source. To change primary data
source update `CSV_SOURCE` environment variable in your `.env`
file.
```
CSV_SOURCE=/path/to/your/csv/file
```

Same goes for `ratings-data.csv` as a ratings data source.
```
CSV_RATINGS_SOURCE=/path/to/your/csv/file
```

If neither primary data source nor ratings data source exist
in specified path application will create them when necessary.

## Tests
All functionality that was not part of a framework is fully
tested. To run the test suite navigate to project directory
and run make test.
```
cd /path/to/gousto-api
make test
```

## Endpoints

#### Fetch recipe by it's ID
```
GET /api/recipes/${id} 
```
Example response:
```
GET /api/recipes/10
 
{
    "message": "Success",
    "data": {
        "id": "10",
        "box_type": "gourmet",
        "title": "Pork Katsu Curry",
        "slug": "pork-katsu-curry",
        "short_title": "",
        "marketing_description": "Comprising all the best bits of the classic American number and none of the mayo, this is a warm & tasty chicken and bulgur salad with just a hint of Scandi influence. A beautifully summery medley of flavours and textures",
        "calories_kcal": "511",
        "protein_grams": "11",
        "fat_grams": "62",
        "carbs_grams": "0",
        "bulletpoint1": "",
        "bulletpoint2": "",
        "bulletpoint3": "",
        "recipe_diet_type_id": "meat",
        "season": "all",
        "base": "",
        "protein_source": "pork",
        "preparation_time_minutes": "45",
        "shelf_life_days": "4",
        "equipment_needed": "Appetite",
        "origin_country": "Great Britain",
        "recipe_cuisine": "mexican",
        "in_your_box": "",
        "gousto_reference": "56"
    }
}
```

Returns http code 200 and recipe's data if `id` exists
in primary data source.

Returns 200 and message `Not Found` if `id` was a positive
integer but there is not recipe with that identifier.

Returns 422 when `id` is not a positive integer that is greater than 0

#### Fetch recipes
```
GET /api/recipes
```
Example response:
```
GET /api/recipes?per_page=3&page=2&cuisine=british

{
    "message": "Success",
    "metadata": {
        "page": 2,
        "per_page": 3,
        "page_count": 1,
        "total_count": 4
    },
    "data": [
        {
            "id": "7",
            "created_at": "02/07/2015 17:58:00",
            "updated_at": "02/07/2015 17:58:00",
            "box_type": "vegetarian",
            "title": "Courgette Pasta Rags",
            "slug": "courgette-pasta-rags",
            "short_title": "",
            "marketing_description": "Kick-start the new year with some get-up and go with this lean green vitality machine. Protein-packed chicken and mineral-rich kale are blended into a smooth, nut-free version of pesto; creating the ultimate composition of nutrition and taste",
            "calories_kcal": "524",
            "protein_grams": "12",
            "fat_grams": "22",
            "carbs_grams": "0",
            "bulletpoint1": "",
            "bulletpoint2": "",
            "bulletpoint3": "",
            "recipe_diet_type_id": "meat",
            "season": "all",
            "base": "",
            "protein_source": "chicken",
            "preparation_time_minutes": "40",
            "shelf_life_days": "4",
            "equipment_needed": "Appetite",
            "origin_country": "Great Britain",
            "recipe_cuisine": "british",
            "in_your_box": "",
            "gousto_reference": "59"
        }
    ]
}
```

Returns http code 200 and data of recipes. By default
pagination is set to 5 and page to 1. Both can be updated
by `?per_page` and `?page` parameter respectively.

Cuisine filter can be applied on search with `?cuisine`
parameter.
#### Store new recipe
```
POST /api/recipes
```

Example response:
```
POST /api/recipes --data "id=100&title=Pizza"

{
    "message": "Success",
    "data": {
        "id": "100",
        "created_at": "",
        "updated_at": "",
        "box_type": "",
        "title": "Pizza",
        "slug": "",
        "short_title": "",
        "marketing_description": "",
        "calories_kcal": "",
        "protein_grams": "",
        "fat_grams": "",
        "carbs_grams": "",
        "bulletpoint1": "",
        "bulletpoint2": "",
        "bulletpoint3": "",
        "recipe_diet_type_id": "",
        "season": "",
        "base": "",
        "protein_source": "",
        "preparation_time_minutes": "",
        "shelf_life_days": "",
        "equipment_needed": "",
        "origin_country": "",
        "recipe_cuisine": "",
        "in_your_box": "",
        "gousto_reference": ""
    }
}
```

Returns http code 200 and recipe's data if `id` is
provided and is a positive integer that is not used
for any other recipe.

Returns 422 when `id` is not provided

Returns 422 when `id` is not a positive integer that is greater than 0

Returns 409 if recipe with `id` already exists in data
store.
#### Update existing recipe
```
PATCH /api/recipes/${id}/update
```
Example response:
```
PATCH /api/recipes/${id}/update --data "carbs_grams=12&bulletpoint1=energy"

{
    "message": "Success",
    "data": {
        "id": "10",
        "created_at": "05/07/2015 17:58:00",
        "updated_at": "05/07/2015 17:58:00",
        "box_type": "gourmet",
        "title": "Pork Katsu Curry",
        "slug": "pork-katsu-curry",
        "short_title": "",
        "marketing_description": "Comprising all the best bits of the classic American number and none of the mayo, this is a warm & tasty chicken and bulgur salad with just a hint of Scandi influence. A beautifully summery medley of flavours and textures",
        "calories_kcal": "511",
        "protein_grams": "11",
        "fat_grams": "62",
        "carbs_grams": "12",
        "bulletpoint1": "energy",
        "bulletpoint2": "",
        "bulletpoint3": "",
        "recipe_diet_type_id": "meat",
        "season": "all",
        "base": "",
        "protein_source": "pork",
        "preparation_time_minutes": "45",
        "shelf_life_days": "4",
        "equipment_needed": "Appetite",
        "origin_country": "Great Britain",
        "recipe_cuisine": "mexican",
        "in_your_box": "",
        "gousto_reference": "56"
    }
}
```
Returns http code 200 and recipe's data if `id` is
provided and is a positive integer that corresponds
to existing recipe in primary data source.

Returns 422 when `id` is not a positive integer that is greater than 0

Returns 422 when `id` doesn't correspond to any recipe
in primary data source.

#### Rate existing recipe between 1 and 5
```
POST /api/recipes/${id}/rate
```
Example response:
```
POST /api/recipes/10/rate --data "rating=5"

{
    "message": "Success",
    "data": {
        "recipe_id": "10",
        "rating": "5"
    }
}
```
Returns http code 200 and rating's data if `id` is
provided and is a positive integer that corresponds
to existing recipe in primary data source.

Returns 422 when `id` is not an integer between 1 and 5

Returns 422 when `id` doesn't correspond to any recipe
in primary data source.