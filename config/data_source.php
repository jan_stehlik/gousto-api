<?php

return [
    'filename' => env('CSV_SOURCE', public_path('recipe-data.csv')),
    'ratings_filename' => env('CSV_RATINGS_SOURCE', public_path('ratings-data.csv')),
];
