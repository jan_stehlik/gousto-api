<?php

use App\Library\CsvHelper;
use Tests\CsvTestCase;

class CsvHelperTest extends CsvTestCase
{
    public function setUp()
    {
        $this->createTestCsvFile('test_data.csv', []);
    }

    public function tearDown()
    {
        unlink('test_data.csv');
    }

    /** @test */
    public function to_array_will_parse_csv_file_into_array()
    {
        $testData = [
            [
                'id' => '1',
                'letter' => 'a',
            ],
            [
                'id' => '2',
                'letter' => 'b',
            ],
        ];
        $this->createTestCsvFile('test_data.csv', $testData);
        $csvHelper = new CsvHelper();

        $data = $csvHelper->toArray('test_data.csv');

        $this->assertEquals([
            [
                'id',
                'letter',
            ],
            [
                '1',
                'a',
            ],
            [
                '2',
                'b',
            ],
        ], $data);
    }

    /** @test */
    public function to_array_will_return_empty_array_if_file_does_not_exist()
    {
        $csvHelper = new CsvHelper();

        $data = $csvHelper->toArray('i_dont_exist.csv');

        $this->assertEquals([], $data);
    }

    /** @test */
    public function fill_data_keys_will_update_array_keys_with_column_names()
    {
        $testData = [
            [
                'id',
                'letter',
            ],
            [
                '1',
                'a',
            ],
            [
                '2',
                'b',
            ],
        ];
        $csvHelper = new CsvHelper();

        $data = $csvHelper->fillDataKeys($testData);

        $this->assertEquals([
            [
                'id' => '1',
                'letter' => 'a',
            ],
            [
                'id' => '2',
                'letter' => 'b',
            ]
        ], $data);
    }

    /** @test */
    public function fill_data_keys_will_return_empty_array_when_no_data_supplied()
    {
        $csvHelper = new CsvHelper();

        $data = $csvHelper->fillDataKeys([]);

        $this->assertEquals([], $data);
    }

    /** @test */
    public function insert_will_insert_both_column_names_and_new_record_data_into_filename_when_no_previous_records_exist()
    {
        $this->createTestCsvFile('test_data.csv', []);
        $newData = [
            'id' => 15,
            'attribute' => 'testing',
        ];
        $csvHelper = new CsvHelper();

        $newRecord = $csvHelper->insert($newData, [], 'test_data.csv');

        $this->assertCsvContains($newData, 'test_data.csv');
        $this->assertEquals($newData, $newRecord);
    }

    /** @test */
    public function insert_will_insert_only_new_record_into_filename_when_there_are_already_some_records()
    {
        $oldData = [
            'id' => 10,
            'attribute' => 'old',
        ];
        $oldRecords = [array_keys($oldData), array_values($oldData)];
        $newData = [
            'id' => 15,
            'attribute' => 'new',
        ];
        $csvHelper = new CsvHelper();

        $newRecord = $csvHelper->insert($newData, $oldRecords, 'test_data.csv');

        $this->assertCsvContains($newData, 'test_data.csv');
        $this->assertCsvContains($oldData, 'test_data.csv');
        $this->assertEquals($newData, $newRecord);
    }

    /** @test */
    public function update_will_update_existing_record_and_save_to_filename()
    {
        $records = [
            [
                'id',
                'attribute',
            ],
            [
                '1',
                'a',
            ],
            [
                '2',
                'b',
            ],
        ];
        $csvHelper = new CsvHelper();

        $updatedRecord = $csvHelper->update('1', ['attribute' => 'NEW'] , $records, 'test_data.csv');

        $this->assertEquals([
            'id' => '1',
            'attribute' => 'NEW',
        ], $updatedRecord);
        $this->assertCsvContains([
            'id' => '1',
            'attribute' => 'NEW',
        ], 'test_data.csv');
        $this->assertCsvContains([
            'id' => '2',
            'attribute' => 'b',
        ], 'test_data.csv');
    }

    /** @test */
    public function cannot_update_will_id_attribute()
    {
        $records = [
            [
                'id',
                'attribute',
            ],
            [
                '1',
                'a',
            ],
            [
                '2',
                'b',
            ],
        ];
        $csvHelper = new CsvHelper();

        $updatedRecord = $csvHelper->update('1', ['id' => 'NEW'] , $records, 'test_data.csv');

        $this->assertEquals([
            'id' => '1',
            'attribute' => 'a',
        ], $updatedRecord);
        $this->assertCsvContains([
            'id' => '1',
            'attribute' => 'a',
        ], 'test_data.csv');
        $this->assertCsvContains([
            'id' => '2',
            'attribute' => 'b',
        ], 'test_data.csv');
    }

    /** @test */
    public function cannot_update_field_that_does_not_exist()
    {
        $records = [
            [
                'id',
                'attribute',
            ],
            [
                '1',
                'a',
            ],
            [
                '2',
                'b',
            ],
        ];
        $csvHelper = new CsvHelper();

        $updatedRecord = $csvHelper->update('1', ['unknown' => 'NEW'] , $records, 'test_data.csv');

        $this->assertEquals([
            'id' => '1',
            'attribute' => 'a',
        ], $updatedRecord);
        $this->assertCsvContains([
            'id' => '1',
            'attribute' => 'a',
        ], 'test_data.csv');
        $this->assertCsvContains([
            'id' => '2',
            'attribute' => 'b',
        ], 'test_data.csv');
    }
}