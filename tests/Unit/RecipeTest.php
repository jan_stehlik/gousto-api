<?php


use App\Recipe;
use Tests\TestCase;

class RecipeTest extends TestCase
{
    /** @test */
    public function find_or_fail_will_return_recipe_when_recipe_id_exists()
    {
        $recipe = Recipe::find(10, [
            [
                'id' => 10,
                'attribute' => 'testing',
            ],
        ]);

        $this->assertEquals((object) [
            'id' => 10,
            'attribute' => 'testing',
        ], $recipe);
    }

    /** @test */
    public function find_or_fail_will_throw_model_not_found_exception_when_recipe_id_does_not_exist()
    {
        $recipe = Recipe::find(10, [
            [
                'id' => 15,
                'attribute' => 'testing',
            ],
        ]);

        $this->assertFalse($recipe);
    }

    /** @test */
    public function get_where_will_return_matches_of_key_value_pair()
    {
        $recipes = [
            [
                'id' => 15,
                'attribute' => 'abc',
            ],
            [
                'id' => 15,
                'attribute' => 'AAA',
            ],
            [
                'id' => 15,
                'attribute' => 'abc',
            ],
        ];

        $matches = Recipe::getWhere('attribute', 'abc', $recipes);

        $this->assertEquals([
            [
                'id' => 15,
                'attribute' => 'abc',
            ],
            [
                'id' => 15,
                'attribute' => 'abc',
            ],
        ], $matches);
    }

    /** @test */
    public function get_where_will_return_empty_array_if_no_matches_found()
    {
        $matches = Recipe::getWhere('attribute', 'AAA', []);

        $this->assertEquals([], $matches);
    }
}