<?php

namespace Tests\Feature;


use Tests\CsvTestCase;

class StoreNewRecipeTest extends CsvTestCase
{
    public function tearDown()
    {
        if (file_exists('test_data.csv')) {
            unlink('test_data.csv');
        }
    }

    /** @test */
    public function id_is_required()
    {
        $this->createTestCsvFile('test_data.csv', []);

        $response = $this->post('/api/recipes', []);

        $response->assertStatus(422);
        $response->assertJson(['message' => 'Unprocessable Entity', 'data' => []]);
        $this->assertCsvEquals([], 'test_data.csv');
    }

    /** @test */
    public function id_must_be_integer()
    {
        $this->createTestCsvFile('test_data.csv', []);

        $response = $this->post('/api/recipes', ['id' => 'not an integer']);

        $response->assertStatus(422);
        $response->assertJson(['message' => 'Unprocessable Entity', 'data' => []]);
        $this->assertCsvEquals([], 'test_data.csv');
    }

    /** @test */
    public function id_must_be_greater_than_0()
    {
        $this->createTestCsvFile('test_data.csv', []);

        $response = $this->post('/api/recipes', ['id' => '0']);

        $response->assertStatus(422);
        $response->assertJson(['message' => 'Unprocessable Entity', 'data' => []]);
        $this->assertCsvEquals([], 'test_data.csv');
    }

    /** @test */
    public function when_attributes_of_recipe_not_specified_default_to_empty_string()
    {
        $this->createTestCsvFile('test_data.csv', []);
        $newRecipeData = [
            'id' => 100,
        ];

        $response = $this->post('/api/recipes', $newRecipeData);

        $response->assertStatus(200);
        $response->assertJson(['message' => 'Success', 'data' => $newRecipeData]);
        $this->assertCsvContains([
            'id' => 100,
            'created_at' => '',
            'updated_at' => '',
            'box_type' => '',
            'title' => '',
            'slug' => '',
            'short_title' => '',
            'marketing_description' => '',
            'calories_kcal' => '',
            'protein_grams' => '',
            'fat_grams' => '',
            'carbs_grams' => '',
            'bulletpoint1' => '',
            'bulletpoint2' => '',
            'bulletpoint3' => '',
            'recipe_diet_type_id' => '',
            'season' => '',
            'base' => '',
            'protein_source' => '',
            'preparation_time_minutes' => '',
            'shelf_life_days' => '',
            'equipment_needed' => '',
            'origin_country' => '',
            'recipe_cuisine' => '',
            'in_your_box' => '',
            'gousto_reference' => '',
        ], 'test_data.csv');
    }

    /** @test */
    public function can_store_new_recipe_when_no_recipe_exists_yet()
    {
        $this->createTestCsvFile('test_data.csv', []);
        $newRecipeData = [
            'id' => 100,
            'created_at' => '1970-01-01 00:00:00',
            'updated_at' => '1970-01-01 00:00:00',
            'box_type' => 'test_box_type',
            'title' => 'test_title',
            'slug' => 'test_slug',
            'short_title' => 'test_short_title',
            'marketing_description' => 'test_marketing_description',
            'calories_kcal' => 'test_calories_kcal',
            'protein_grams' => 'test_protein_grams',
            'fat_grams' => 'test_fat_grams',
            'carbs_grams' => 'test_carbs_grams',
            'bulletpoint1' => 'test_bulletpoint1',
            'bulletpoint2' => 'test_bulletpoint2',
            'bulletpoint3' => 'test_bulletpoint3',
            'recipe_diet_type_id' => 'test_recipe_diet_type_id',
            'season' => 'test_season',
            'base' => 'test_base',
            'protein_source' => 'test_protein_source',
            'preparation_time_minutes' => 'test_preparation_time_minutes',
            'shelf_life_days' => 'test_shelf_life_days',
            'equipment_needed' => 'test_equipment_needed',
            'origin_country' => 'test_origin_country',
            'recipe_cuisine' => 'test_recipe_cuisine',
            'in_your_box' => 'test_in_your_box',
            'gousto_reference' => 'test_gousto_reference',
        ];

        $response = $this->post('/api/recipes', $newRecipeData);

        $response->assertStatus(200);
        $response->assertJson(['message' => 'Success', 'data' => $newRecipeData]);
        $this->assertCsvContains($newRecipeData, 'test_data.csv');
    }

    /** @test */
    public function can_store_new_recipe_when_recipes_exist_already()
    {
        $oldData = [
            'id' => 99,
            'created_at' => '1970-01-01 00:00:00',
            'updated_at' => '1970-01-01 00:00:00',
            'box_type' => 'test_box_type A',
            'title' => 'test_title A',
            'slug' => 'test_slug A',
            'short_title' => 'test_short_title A',
            'marketing_description' => 'test_marketing_description A',
            'calories_kcal' => 'test_calories_kcal A',
            'protein_grams' => 'test_protein_grams A',
            'fat_grams' => 'test_fat_grams A',
            'carbs_grams' => 'test_carbs_grams A',
            'bulletpoint1' => 'test_bulletpoint1 A',
            'bulletpoint2' => 'test_bulletpoint2 A',
            'bulletpoint3' => 'test_bulletpoint3 A',
            'recipe_diet_type_id' => 'test_recipe_diet_type_id A',
            'season' => 'test_season A',
            'base' => 'test_base A',
            'protein_source' => 'test_protein_source A',
            'preparation_time_minutes' => 'test_preparation_time_minutes A',
            'shelf_life_days' => 'test_shelf_life_days A',
            'equipment_needed' => 'test_equipment_needed A',
            'origin_country' => 'test_origin_country A',
            'recipe_cuisine' => 'test_recipe_cuisine A',
            'in_your_box' => 'test_in_your_box A',
            'gousto_reference' => 'test_gousto_reference A',
        ];
        $this->createTestCsvFile('test_data.csv', [$oldData]);

        $newRecipeData = [
            'id' => 100,
            'created_at' => '1970-01-01 00:00:00',
            'updated_at' => '1970-01-01 00:00:00',
            'box_type' => 'test_box_type B',
            'title' => 'test_title B',
            'slug' => 'test_slug B',
            'short_title' => 'test_short_title B',
            'marketing_description' => 'test_marketing_description B',
            'calories_kcal' => 'test_calories_kcal B',
            'protein_grams' => 'test_protein_grams B',
            'fat_grams' => 'test_fat_grams B',
            'carbs_grams' => 'test_carbs_grams B',
            'bulletpoint1' => 'test_bulletpoint1 B',
            'bulletpoint2' => 'test_bulletpoint2 B',
            'bulletpoint3' => 'test_bulletpoint3 B',
            'recipe_diet_type_id' => 'test_recipe_diet_type_id B',
            'season' => 'test_season B',
            'base' => 'test_base B',
            'protein_source' => 'test_protein_source B',
            'preparation_time_minutes' => 'test_preparation_time_minutes B',
            'shelf_life_days' => 'test_shelf_life_days B',
            'equipment_needed' => 'test_equipment_needed B',
            'origin_country' => 'test_origin_country B',
            'recipe_cuisine' => 'test_recipe_cuisine B',
            'in_your_box' => 'test_in_your_box B',
            'gousto_reference' => 'test_gousto_reference B',
        ];

        $response = $this->post('/api/recipes', $newRecipeData);

        $response->assertStatus(200);
        $response->assertJson(['message' => 'Success', 'data' => $newRecipeData]);
        $this->assertCsvContains($oldData, 'test_data.csv');
        $this->assertCsvContains($newRecipeData, 'test_data.csv');
    }

    /** @test */
    public function cannot_store_recipe_if_redice_id_already_exists()
    {
        $oldData = [
            'id' => 100,
            'created_at' => '1970-01-01 00:00:00',
            'updated_at' => '1970-01-01 00:00:00',
            'box_type' => 'test_box_type A',
            'title' => 'test_title A',
            'slug' => 'test_slug A',
            'short_title' => 'test_short_title A',
            'marketing_description' => 'test_marketing_description A',
            'calories_kcal' => 'test_calories_kcal A',
            'protein_grams' => 'test_protein_grams A',
            'fat_grams' => 'test_fat_grams A',
            'carbs_grams' => 'test_carbs_grams A',
            'bulletpoint1' => 'test_bulletpoint1 A',
            'bulletpoint2' => 'test_bulletpoint2 A',
            'bulletpoint3' => 'test_bulletpoint3 A',
            'recipe_diet_type_id' => 'test_recipe_diet_type_id A',
            'season' => 'test_season A',
            'base' => 'test_base A',
            'protein_source' => 'test_protein_source A',
            'preparation_time_minutes' => 'test_preparation_time_minutes A',
            'shelf_life_days' => 'test_shelf_life_days A',
            'equipment_needed' => 'test_equipment_needed A',
            'origin_country' => 'test_origin_country A',
            'recipe_cuisine' => 'test_recipe_cuisine A',
            'in_your_box' => 'test_in_your_box A',
            'gousto_reference' => 'test_gousto_reference A',
        ];
        $this->createTestCsvFile('test_data.csv', [$oldData]);

        $newRecipeData = [
            'id' => 100,
            'created_at' => '1970-01-01 00:00:00',
            'updated_at' => '1970-01-01 00:00:00',
            'box_type' => 'test_box_type B',
            'title' => 'test_title B',
            'slug' => 'test_slug B',
            'short_title' => 'test_short_title B',
            'marketing_description' => 'test_marketing_description B',
            'calories_kcal' => 'test_calories_kcal B',
            'protein_grams' => 'test_protein_grams B',
            'fat_grams' => 'test_fat_grams B',
            'carbs_grams' => 'test_carbs_grams B',
            'bulletpoint1' => 'test_bulletpoint1 B',
            'bulletpoint2' => 'test_bulletpoint2 B',
            'bulletpoint3' => 'test_bulletpoint3 B',
            'recipe_diet_type_id' => 'test_recipe_diet_type_id B',
            'season' => 'test_season B',
            'base' => 'test_base B',
            'protein_source' => 'test_protein_source B',
            'preparation_time_minutes' => 'test_preparation_time_minutes B',
            'shelf_life_days' => 'test_shelf_life_days B',
            'equipment_needed' => 'test_equipment_needed B',
            'origin_country' => 'test_origin_country B',
            'recipe_cuisine' => 'test_recipe_cuisine B',
            'in_your_box' => 'test_in_your_box B',
            'gousto_reference' => 'test_gousto_reference B',
        ];

        $response = $this->post('/api/recipes', $newRecipeData);

        $response->assertStatus(409);
        $response->assertJson(['message' => 'Conflict', 'data' => []]);
        $this->assertCsvContains($oldData, 'test_data.csv');
        $this->assertCsvNotContains($newRecipeData, 'test_data.csv');
    }
}