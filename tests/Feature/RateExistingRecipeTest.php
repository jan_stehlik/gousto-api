<?php

namespace Tests\Feature;


use App\Library\RecipeFactory;
use Tests\CsvTestCase;

class RateExistingRecipeTest extends CsvTestCase
{
    public function tearDown()
    {
        if (file_exists('test_data.csv')) {
            unlink('test_data.csv');
        }

        if (file_exists('test_ratings.csv')) {
            unlink('test_ratings.csv');
        }
    }

    /** @test */
    public function will_return_422_when_recipe_id_is_not_an_integer()
    {
        $response = $this->post('/api/recipes/not_integer/rate', []);

        $response->assertStatus(422);
        $response->assertExactJson([
            'message' => 'Unprocessable Entity',
            'data' => []
        ]);
    }

    /** @test */
    public function will_return_422_when_recipe_id_is_not_greater_than_0()
    {
        $response = $this->post('/api/recipes/0/rate', []);

        $response->assertStatus(422);
        $response->assertExactJson([
            'message' => 'Unprocessable Entity',
            'data' => []
        ]);
    }

    /** @test */
    public function will_return_422_when_rating_not_specified()
    {
        $recipe = RecipeFactory::create(['id' => '100']);
        $this->createTestCsvFile('test_data.csv', [$recipe]);

        $response = $this->post('/api/recipes/100/rate', []);

        $response->assertStatus(422);
        $response->assertExactJson([
            'message' => 'Unprocessable Entity',
            'data' => []
        ]);
    }

    /** @test */
    public function cannot_rate_non_existant_recipe()
    {
        $this->createTestCsvFile('test_data.csv', []);

        $response = $this->post('/api/recipes/100/rate', ['rating' => '3']);

        $response->assertStatus(422);
        $response->assertExactJson([
            'message' => 'Unprocessable Entity',
            'data' => []
        ]);
        $this->assertCsvEquals([], 'test_ratings.csv');
    }

    /** @test */
    public function existing_recipe_id_can_be_given_rating()
    {
        $recipe = RecipeFactory::create(['id' => '100']);
        $this->createTestCsvFile('test_data.csv', [$recipe]);
        $this->createTestCsvFile('test_ratings.csv', []);

        $response = $this->post('/api/recipes/100/rate', ['rating' => '3']);

        $response->assertStatus(200);
        $response->assertExactJson([
            'message' => 'Success',
            'data' => [
                'recipe_id' => '100',
                'rating' => '3'
            ]
        ]);
        $this->assertCsvEquals([
            [
                'recipe_id' => '100',
                'rating' => '3'
            ]
        ], 'test_ratings.csv');
    }

    /** @test */
    public function rating_must_be_integer()
    {
        $recipe = RecipeFactory::create(['id' => '100']);
        $this->createTestCsvFile('test_data.csv', [$recipe]);
        $this->createTestCsvFile('test_ratings.csv', []);

        $response = $this->post('/api/recipes/100/rate', ['rating' => 'bla']);

        $response->assertStatus(422);
        $response->assertExactJson([
            'message' => 'Unprocessable Entity',
            'data' => []
        ]);
        $this->assertCsvEquals([], 'test_ratings.csv');
    }

    /** @test */
    public function rating_must_be_between_1_and_5()
    {
        $recipe = RecipeFactory::create(['id' => '100']);
        $this->createTestCsvFile('test_data.csv', [$recipe]);
        $this->createTestCsvFile('test_ratings.csv', []);

        $response = $this->post('/api/recipes/100/rate', ['rating' => '6']);

        $response->assertStatus(422);
        $response->assertExactJson([
            'message' => 'Unprocessable Entity',
            'data' => []
        ]);
        $this->assertCsvEquals([], 'test_ratings.csv');
    }
}