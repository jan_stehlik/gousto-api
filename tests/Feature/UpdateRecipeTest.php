<?php

namespace Tests\Feature;


use App\Library\RecipeFactory;
use Tests\CsvTestCase;

class UpdateRecipeTest extends CsvTestCase
{
    public function tearDown()
    {
        if (file_exists('test_data.csv')) {
            unlink('test_data.csv');
        }
    }

    /** @test */
    public function will_return_422_when_recipe_id_is_not_an_integer()
    {
        $response = $this->patch('/api/recipes/not_integer/update', []);

        $response->assertStatus(422);
        $response->assertExactJson([
            'message' => 'Unprocessable Entity',
            'data' => []
        ]);
    }

    /** @test */
    public function will_return_422_when_recipe_id_is_not_greater_than_0()
    {
        $response = $this->patch('/api/recipes/0/update', []);

        $response->assertStatus(422);
        $response->assertExactJson([
            'message' => 'Unprocessable Entity',
            'data' => []
        ]);
    }

    /** @test */
    public function attempt_to_update_attributes_of_non_existant_recipe_will_return_422()
    {
        $this->createTestCsvFile('test_data.csv', []);

        $response = $this->patch('/api/recipes/100/update', []);

        $response->assertStatus(422);
        $response->assertExactJson([
            'message' => 'Unprocessable Entity',
            'data' => []
        ]);
        $this->assertCsvEquals([], 'test_ratings.csv');
    }

    /** @test */
    public function can_update_attributes_of_existing_recipe()
    {
        $recipe = RecipeFactory::create(['id' => '100']);
        $this->createTestCsvFile('test_data.csv', [$recipe]);

        $response = $this->patch('/api/recipes/100/update', ['title' => 'test title']);

        $response->assertStatus(200);
        $response->assertJson([
            'message' => 'Success',
            'data' => [
                'id' => '100',
                'title' => 'test title'
            ]
        ]);
        $this->assertCsvContains([
            'id' => '100',
            'created_at' => '1970-01-01 00:00:00',
            'updated_at' => '1970-01-01 00:00:00',
            'box_type' => '',
            'title' => 'test title',
            'slug' => '',
            'short_title' => '',
            'marketing_description' => '',
            'calories_kcal' => '',
            'protein_grams' => '',
            'fat_grams' => '',
            'carbs_grams' => '',
            'bulletpoint1' => '',
            'bulletpoint2' => '',
            'bulletpoint3' => '',
            'recipe_diet_type_id' => '',
            'season' => '',
            'base' => '',
            'protein_source' => '',
            'preparation_time_minutes' => '',
            'shelf_life_days' => '',
            'equipment_needed' => '',
            'origin_country' => '',
            'recipe_cuisine' => '',
            'in_your_box' => '',
            'gousto_reference' => '',
        ], 'test_data.csv');
    }

    /** @test */
    public function can_update_multiple_attributes_of_existing_recipe()
    {
        $recipe = RecipeFactory::create(['id' => '100']);
        $this->createTestCsvFile('test_data.csv', [$recipe]);

        $response = $this->patch(
            '/api/recipes/100/update',
            [
                'title' => 'test title',
                'season' => 'all',
                'base' => 'beef',
            ]
        );

        $response->assertStatus(200);
        $response->assertJson([
            'message' => 'Success',
            'data' => [
                'id' => '100',
                'title' => 'test title',
                'season' => 'all',
                'base' => 'beef',
            ]
        ]);
        $this->assertCsvContains([
            'id' => '100',
            'created_at' => '1970-01-01 00:00:00',
            'updated_at' => '1970-01-01 00:00:00',
            'box_type' => '',
            'title' => 'test title',
            'slug' => '',
            'short_title' => '',
            'marketing_description' => '',
            'calories_kcal' => '',
            'protein_grams' => '',
            'fat_grams' => '',
            'carbs_grams' => '',
            'bulletpoint1' => '',
            'bulletpoint2' => '',
            'bulletpoint3' => '',
            'recipe_diet_type_id' => '',
            'season' => 'all',
            'base' => 'beef',
            'protein_source' => '',
            'preparation_time_minutes' => '',
            'shelf_life_days' => '',
            'equipment_needed' => '',
            'origin_country' => '',
            'recipe_cuisine' => '',
            'in_your_box' => '',
            'gousto_reference' => '',
        ], 'test_data.csv');
    }

    /** @test */
    public function cannot_update_id_of_existing_recipe()
    {
        $recipe = RecipeFactory::create(['id' => '100']);
        $this->createTestCsvFile('test_data.csv', [$recipe]);

        $response = $this->patch('/api/recipes/100/update', ['id' => '999']);

        $response->assertStatus(200);
        $response->assertJson([
            'message' => 'Success',
            'data' => [
                'id' => '100'
            ]
        ]);
        $this->assertCsvContains([
            'id' => '100',
            'created_at' => '1970-01-01 00:00:00',
            'updated_at' => '1970-01-01 00:00:00',
            'box_type' => '',
            'title' => '',
            'slug' => '',
            'short_title' => '',
            'marketing_description' => '',
            'calories_kcal' => '',
            'protein_grams' => '',
            'fat_grams' => '',
            'carbs_grams' => '',
            'bulletpoint1' => '',
            'bulletpoint2' => '',
            'bulletpoint3' => '',
            'recipe_diet_type_id' => '',
            'season' => '',
            'base' => '',
            'protein_source' => '',
            'preparation_time_minutes' => '',
            'shelf_life_days' => '',
            'equipment_needed' => '',
            'origin_country' => '',
            'recipe_cuisine' => '',
            'in_your_box' => '',
            'gousto_reference' => '',
        ], 'test_data.csv');
        $this->assertCsvNotContains([
            'id' => '999',
            'created_at' => '1970-01-01 00:00:00',
            'updated_at' => '1970-01-01 00:00:00',
            'box_type' => '',
            'title' => '',
            'slug' => '',
            'short_title' => '',
            'marketing_description' => '',
            'calories_kcal' => '',
            'protein_grams' => '',
            'fat_grams' => '',
            'carbs_grams' => '',
            'bulletpoint1' => '',
            'bulletpoint2' => '',
            'bulletpoint3' => '',
            'recipe_diet_type_id' => '',
            'season' => 'all',
            'base' => 'beef',
            'protein_source' => '',
            'preparation_time_minutes' => '',
            'shelf_life_days' => '',
            'equipment_needed' => '',
            'origin_country' => '',
            'recipe_cuisine' => '',
            'in_your_box' => '',
            'gousto_reference' => '',
        ], 'test_data.csv');
    }
}