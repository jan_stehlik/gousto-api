<?php

namespace Tests\Feature;


use App\Library\RecipeFactory;
use Tests\CsvTestCase;

class GetRecipeTest extends CsvTestCase
{
    public function tearDown()
    {
        if (file_exists('test_data.csv')) {
            unlink('test_data.csv');
        }
    }

    /** @test */
    public function get_recipe_by_id_will_return_client_error_when_recipe_id_is_not_an_integer()
    {
        $response = $this->get('/api/recipes/not_an_integer');

        $response->assertStatus(422);
        $response->assertJson(['message' => 'Unprocessable Entity', 'data' => null]);
    }

    /** @test */
    public function get_recipe_by_id_will_return_client_error_when_recipe_id_is_not_greater_than_zero()
    {
        $response = $this->get('/api/recipes/0');

        $response->assertStatus(422);
        $response->assertJson(['message' => 'Unprocessable Entity', 'data' => null]);
    }

    /** @test */
    public function get_recipe_by_id_will_return_response_with_correct_recipe_data_when_recipe_with_given_id_does_exist()
    {
        $recipe = RecipeFactory::create([
            'box_type' => 'test_box_type',
            'title' => 'test_title',
            'slug' => 'test_slug',
            'short_title' => 'test_short_title',
            'marketing_description' => 'test_marketing_description',
            'calories_kcal' => 'test_calories_kcal',
            'protein_grams' => 'test_protein_grams',
            'fat_grams' => 'test_fat_grams',
            'carbs_grams' => 'test_carbs_grams',
            'bulletpoint1' => 'test_bulletpoint1',
            'bulletpoint2' => 'test_bulletpoint2',
            'bulletpoint3' => 'test_bulletpoint3',
            'recipe_diet_type_id' => 'test_recipe_diet_type_id',
            'season' => 'test_season',
            'base' => 'test_base',
            'protein_source' => 'test_protein_source',
            'preparation_time_minutes' => 'test_preparation_time_minutes',
            'shelf_life_days' => 'test_shelf_life_days',
            'equipment_needed' => 'test_equipment_needed',
            'origin_country' => 'test_origin_country',
            'recipe_cuisine' => 'test_recipe_cuisine',
            'in_your_box' => 'test_in_your_box',
            'gousto_reference' => 'test_gousto_reference',
        ]);
        $this->createTestCsvFile('test_data.csv', [$recipe]);

        $response = $this->get('/api/recipes/' . $recipe['id']);

        $response->assertStatus(200);
        $response->assertJson([
            'message' => 'Success',
            'data' => [
                'id' => $recipe['id'],
                'box_type' => 'test_box_type',
                'title' => 'test_title',
                'slug' => 'test_slug',
                'short_title' => 'test_short_title',
                'marketing_description' => 'test_marketing_description',
                'calories_kcal' => 'test_calories_kcal',
                'protein_grams' => 'test_protein_grams',
                'fat_grams' => 'test_fat_grams',
                'carbs_grams' => 'test_carbs_grams',
                'bulletpoint1' => 'test_bulletpoint1',
                'bulletpoint2' => 'test_bulletpoint2',
                'bulletpoint3' => 'test_bulletpoint3',
                'recipe_diet_type_id' => 'test_recipe_diet_type_id',
                'season' => 'test_season',
                'base' => 'test_base',
                'protein_source' => 'test_protein_source',
                'preparation_time_minutes' => 'test_preparation_time_minutes',
                'shelf_life_days' => 'test_shelf_life_days',
                'equipment_needed' => 'test_equipment_needed',
                'origin_country' => 'test_origin_country',
                'recipe_cuisine' => 'test_recipe_cuisine',
                'in_your_box' => 'test_in_your_box',
                'gousto_reference' => 'test_gousto_reference',
            ]
        ]);
    }

    /** @test */
    public function get_recipe_by_id_will_return_response_with_message_not_found_when_recipe_with_given_id_does_not_exist()
    {
        $recipe = RecipeFactory::create([
            'id' => 10,
        ]);
        $this->createTestCsvFile('test_data.csv', [$recipe]);

        $response = $this->get('/api/recipes/1');

        $response->assertStatus(200);
        $response->assertJson([
            'message' => 'Not Found',
            'data' => []
        ]);
    }

    /** @test */
    public function get_recipe_by_id_will_return_response_with_message_not_found_when_no_recipes_exist()
    {
        $this->createTestCsvFile('test_data.csv', []);

        $response = $this->get('/api/recipes/1');

        $response->assertStatus(200);
        $response->assertJson([
            'message' => 'Not Found',
            'data' => []
        ]);
    }
}