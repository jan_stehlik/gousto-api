<?php

namespace Tests\Feature;


use App\Library\RecipeFactory;
use Tests\CsvTestCase;

class GetRecipesTest extends CsvTestCase
{
    public function tearDown()
    {
        if (file_exists('test_data.csv')) {
            unlink('test_data.csv');
        }
    }

    /** @test */
    public function get_recipes_will_return_page_1_of_recipes_paginated_by_default_to_5_when_no_per_page_defined()
    {
        $recipes[0] = RecipeFactory::create(['id' => '100']);
        $recipes[1] = RecipeFactory::create(['id' => '200']);
        $recipes[2] = RecipeFactory::create(['id' => '300']);
        $recipes[3] = RecipeFactory::create(['id' => '400']);
        $recipes[4] = RecipeFactory::create(['id' => '500']);
        $recipes[5] = RecipeFactory::create(['id' => '600']);
        $this->createTestCsvFile('test_data.csv', $recipes);

        $response = $this->get('/api/recipes');

        $response->assertStatus(200);
        $response->assertExactJson([
            'message' => 'Success',
            'metadata' => [
                'page' => 1,
                'per_page' => 5,
                'page_count' => 5,
                'total_count' => 6,
            ],
            'data' => [
                $recipes[0],
                $recipes[1],
                $recipes[2],
                $recipes[3],
                $recipes[4],
            ]
        ]);
    }

    /** @test */
    public function get_recipes_will_return_page_1_of_recipes_paginated_by_custom_pagination_when_per_page_defined()
    {
        $recipes[0] = RecipeFactory::create(['id' => '100']);
        $recipes[1] = RecipeFactory::create(['id' => '200']);
        $recipes[2] = RecipeFactory::create(['id' => '300']);
        $recipes[3] = RecipeFactory::create(['id' => '400']);
        $recipes[4] = RecipeFactory::create(['id' => '500']);
        $recipes[5] = RecipeFactory::create(['id' => '600']);
        $this->createTestCsvFile('test_data.csv', $recipes);

        $response = $this->get('/api/recipes?per_page=3');

        $response->assertStatus(200);
        $response->assertExactJson([
            'message' => 'Success',
            'metadata' => [
                'page' => 1,
                'per_page' => 3,
                'page_count' => 3,
                'total_count' => 6,
            ],
            'data' => [
                $recipes[0],
                $recipes[1],
                $recipes[2],
            ]
        ]);
    }

    /** @test */
    public function get_recipes_will_return_custom_page_of_recipes_when_page_defined()
    {
        $recipes[0] = RecipeFactory::create(['id' => '100']);
        $recipes[1] = RecipeFactory::create(['id' => '200']);
        $recipes[2] = RecipeFactory::create(['id' => '300']);
        $recipes[3] = RecipeFactory::create(['id' => '400']);
        $recipes[4] = RecipeFactory::create(['id' => '500']);
        $recipes[5] = RecipeFactory::create(['id' => '600']);
        $this->createTestCsvFile('test_data.csv', $recipes);

        $response = $this->get('/api/recipes?page=2');

        $response->assertStatus(200);
        $response->assertExactJson([
            'message' => 'Success',
            'metadata' => [
                'page' => 2,
                'per_page' => 5,
                'page_count' => 1,
                'total_count' => 6,
            ],
            'data' => [
                $recipes[5],
            ]
        ]);
    }

    /** @test */
    public function get_recipes_will_return_custom_page_of_custom_pagination_when_page_and_per_page_defined()
    {
        $recipes[0] = RecipeFactory::create(['id' => '100']);
        $recipes[1] = RecipeFactory::create(['id' => '200']);
        $recipes[2] = RecipeFactory::create(['id' => '300']);
        $recipes[3] = RecipeFactory::create(['id' => '400']);
        $recipes[4] = RecipeFactory::create(['id' => '500']);
        $recipes[5] = RecipeFactory::create(['id' => '600']);
        $this->createTestCsvFile('test_data.csv', $recipes);

        $response = $this->get('/api/recipes?page=2&per_page=3');

        $response->assertStatus(200);
        $response->assertExactJson([
            'message' => 'Success',
            'metadata' => [
                'page' => 2,
                'per_page' => 3,
                'page_count' => 3,
                'total_count' => 6,
            ],
            'data' => [
                $recipes[3],
                $recipes[4],
                $recipes[5],
            ]
        ]);
    }

    /** @test */
    public function get_recipes_will_return_message_not_found_when_data_does_not_exist()
    {
        $recipes[0] = RecipeFactory::create(['id' => '100']);
        $recipes[1] = RecipeFactory::create(['id' => '200']);
        $recipes[2] = RecipeFactory::create(['id' => '300']);
        $recipes[3] = RecipeFactory::create(['id' => '400']);
        $recipes[4] = RecipeFactory::create(['id' => '500']);
        $recipes[5] = RecipeFactory::create(['id' => '600']);
        $this->createTestCsvFile('test_data.csv', $recipes);

        $response = $this->get('/api/recipes?page=5&per_page=10');

        $response->assertStatus(200);
        $response->assertExactJson([
            'message' => 'Not Found',
            'metadata' => [
                'page' => 5,
                'per_page' => 10,
                'page_count' => 0,
                'total_count' => 6,
            ],
            'data' => []
        ]);
    }

    /** @test */
    public function get_recipes_will_return_recipes_for_cuisine_if_cuisine_parameter_defined()
    {
        $recipes[0] = RecipeFactory::create(['id' => '100', 'recipe_cuisine' => 'B']);
        $recipes[1] = RecipeFactory::create(['id' => '200', 'recipe_cuisine' => 'A']);
        $recipes[2] = RecipeFactory::create(['id' => '300', 'recipe_cuisine' => 'A']);
        $recipes[3] = RecipeFactory::create(['id' => '400', 'recipe_cuisine' => 'A']);
        $recipes[4] = RecipeFactory::create(['id' => '500', 'recipe_cuisine' => 'B']);
        $recipes[5] = RecipeFactory::create(['id' => '600', 'recipe_cuisine' => 'A']);
        $this->createTestCsvFile('test_data.csv', $recipes);

        $response = $this->get('/api/recipes?cuisine=A');

        $response->assertStatus(200);
        $response->assertExactJson([
            'message' => 'Success',
            'metadata' => [
                'page' => 1,
                'per_page' => 5,
                'page_count' => 4,
                'total_count' => 4,
            ],
            'data' => [
                $recipes[1],
                $recipes[2],
                $recipes[3],
                $recipes[5],
            ]
        ]);
    }
}