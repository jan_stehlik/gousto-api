<?php

namespace Tests;


use App\Library\CsvHelper;

class CsvTestCase extends TestCase
{
    protected function createTestCsvFile($filename, $testData)
    {
        $fp = fopen($filename, 'w');

        if (empty($testData)) {
            fclose($fp);
            return;
        }

        $columnNames = array_keys($testData[0]);
        fputcsv($fp, $columnNames, ',');

        foreach ($testData as $row) {
            $rowData = array_values($row);
            fputcsv($fp, $rowData, ',');
        }

        fclose($fp);
    }

    protected function assertCsvContains($expected, $filename)
    {
        $csvParser = new CsvHelper();
        $csvData = $csvParser->toArray($filename);
        $csvData = $csvParser->fillDataKeys($csvData);

        $this->assertContains($expected, $csvData);
    }

    protected function assertCsvNotContains($expected, $filename)
    {
        $csvParser = new CsvHelper();
        $csvData = $csvParser->toArray($filename);
        $csvData = $csvParser->fillDataKeys($csvData);

        $this->assertNotContains($expected, $csvData);
    }

    protected function assertCsvEquals($expected, $filename)
    {
        $csvParser = new CsvHelper();
        $csvData = $csvParser->toArray($filename);
        $csvData = $csvParser->fillDataKeys($csvData);

        $this->assertEquals($expected, $csvData);
    }
}