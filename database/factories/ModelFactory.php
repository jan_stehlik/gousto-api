<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Recipe::class, function (Faker $faker) {
    return [
        'box_type' => null,
        'title' => null,
        'slug' => null,
        'short_title' => null,
        'marketing_description' => null,
        'calories_kcal' => null,
        'protein_grams' => null,
        'fat_grams' => null,
        'carbs_grams' => null,
        'bulletpoint1' => null,
        'bulletpoint2' => null,
        'bulletpoint3' => null,
        'recipe_diet_type_id' => null,
        'season' => null,
        'base' => null,
        'protein_source' => null,
        'preparation_time_minutes' => null,
        'shelf_life_days' => null,
        'equipment_needed' => null,
        'origin_country' => null,
        'recipe_cuisine' => null,
        'in_your_box' => null,
        'gousto_reference' => null,
    ];
});
