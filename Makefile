start: install run

install:
	composer install

run:
	php artisan serve

test:
	./vendor/phpunit/phpunit/phpunit

.PHONY: start install run test