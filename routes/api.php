<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/recipes/{id}', 'RecipeController@getById')->name('getRecipeById');
Route::get('/recipes', 'RecipeController@get')->name('getRecipes');
Route::post('/recipes', 'RecipeController@store')->name('storeRecipe');
Route::post('/recipes/{id}/rate', 'RecipeController@rate')->name('rateRecipe');
Route::patch('/recipes/{id}/update', 'RecipeController@update')->name('updateRecipe');
