<?php

namespace App\Http\Controllers;

use App\Library\CsvHelper;
use App\Recipe;
use Illuminate\Http\Request;

class RecipeController extends Controller
{
    protected $csvHelper;

    public function __construct(CsvHelper $csvHelper)
    {
        $this->csvHelper = $csvHelper;
    }

    public function getById($id)
    {
        if ((!is_int($id) && !ctype_digit($id)) || $id <= 0) {
            return response(['message' => 'Unprocessable Entity', 'data' => []], 422);
        }

        $recipes = $this->csvHelper->toArray(config('data_source.filename'));
        $recipes = $this->csvHelper->fillDataKeys($recipes);

        if ($recipe = Recipe::find($id, $recipes)) {
            $message = 'Success';
            $data = [
                'id' => $recipe->id,
                'box_type' => $recipe->box_type,
                'title' => $recipe->title,
                'slug' => $recipe->slug,
                'short_title' => $recipe->short_title,
                'marketing_description' => $recipe->marketing_description,
                'calories_kcal' => $recipe->calories_kcal,
                'protein_grams' => $recipe->protein_grams,
                'fat_grams' => $recipe->fat_grams,
                'carbs_grams' => $recipe->carbs_grams,
                'bulletpoint1' => $recipe->bulletpoint1,
                'bulletpoint2' => $recipe->bulletpoint2,
                'bulletpoint3' => $recipe->bulletpoint3,
                'recipe_diet_type_id' => $recipe->recipe_diet_type_id,
                'season' => $recipe->season,
                'base' => $recipe->base,
                'protein_source' => $recipe->protein_source,
                'preparation_time_minutes' => $recipe->preparation_time_minutes,
                'shelf_life_days' => $recipe->shelf_life_days,
                'equipment_needed' => $recipe->equipment_needed,
                'origin_country' => $recipe->origin_country,
                'recipe_cuisine' => $recipe->recipe_cuisine,
                'in_your_box' => $recipe->in_your_box,
                'gousto_reference' => $recipe->gousto_reference,
            ];
        } else {
            $message = 'Not Found';
            $data = [];
        }

        return response(['message' => $message, 'data' => $data], 200);
    }

    public function get(Request $request)
    {
        $perPage = $request->has('per_page') ? (int) $request->get('per_page') : 5;
        $page = $request->has('page') ? (int) $request->get('page') : 1;
        $offset = $page * $perPage - $perPage;

        $recipes = $this->csvHelper->toArray(config('data_source.filename'));
        $recipes = $this->csvHelper->fillDataKeys($recipes);

        if ($request->has('cuisine')) {
            $recipes = Recipe::getWhere('recipe_cuisine', $request->get('cuisine'), $recipes);
        }

        $paginated = array_slice($recipes, $offset, $perPage);
        $message = empty($paginated) ? 'Not Found' : 'Success';

        return response([
            'message' => $message,
            'metadata' => [
                'page' => $page,
                'per_page' => $perPage,
                'page_count' => count($paginated),
                'total_count' => count($recipes),
            ],
            'data' => $paginated
        ], 200);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'id' => 'required|integer|min:1',
            'created_at' => '',
            'updated_at' => '',
            'box_type' => '',
            'title' => '',
            'slug' => '',
            'short_title' => '',
            'marketing_description' => '',
            'calories_kcal' => '',
            'protein_grams' => '',
            'fat_grams' => '',
            'carbs_grams' => '',
            'bulletpoint1' => '',
            'bulletpoint2' => '',
            'bulletpoint3' => '',
            'recipe_diet_type_id' => '',
            'season' => '',
            'base' => '',
            'protein_source' => '',
            'preparation_time_minutes' => '',
            'shelf_life_days' => '',
            'equipment_needed' => '',
            'origin_country' => '',
            'recipe_cuisine' => '',
            'in_your_box' => '',
            'gousto_reference' => '',
        ]);
        $data = array_merge(Recipe::getProperties(), $validatedData);

        $csvFile = config('data_source.filename');
        $recipes = $this->csvHelper->toArray(config('data_source.filename'));

        if (Recipe::find($data['id'], $this->csvHelper->fillDataKeys($recipes))) {
            return response(['message' => 'Conflict', 'data' => []], 409);
        }
        $recipe = $this->csvHelper->insert($data, $recipes, $csvFile);

        return response(['message' => 'Success', 'data' => $recipe], 200);
    }

    public function update($id, Request $request)
    {
        if ((!is_int($id) && !ctype_digit($id)) || $id <= 0) {
            return response(['message' => 'Unprocessable Entity', 'data' => []], 422);
        }

        $csvFile = config('data_source.filename');
        $recipes = $this->csvHelper->toArray($csvFile);
        if (!Recipe::find($id, $this->csvHelper->fillDataKeys($recipes))) {
            return response(['message' => 'Unprocessable Entity', 'data' => []], 422);
        }

        $data = $request->all();
        $recipe = $this->csvHelper->update($id, $data, $recipes, $csvFile);

        return response(['message' => 'Success', 'data' => $recipe], 200);
    }

    public function rate($id, Request $request)
    {
        if ((!is_int($id) && !ctype_digit($id)) || $id <= 0 || !$request->has('rating')) {
            return response(['message' => 'Unprocessable Entity', 'data' => []], 422);
        }

        $validatedData = $request->validate([
            'rating' => 'required|integer|min:1|max:5',
        ]);

        $csvFile = config('data_source.ratings_filename');
        $ratings = $this->csvHelper->toArray($csvFile);

        $recipes = $this->csvHelper->toArray(config('data_source.filename'));
        if (!Recipe::find($id, $this->csvHelper->fillDataKeys($recipes))) {
            return response(['message' => 'Unprocessable Entity', 'data' => []], 422);
        }

        $data = [
            'recipe_id' => $id,
            'rating' => $validatedData['rating'],
        ];

        $rating = $this->csvHelper->insert($data, $ratings, $csvFile);

        return response(['message' => 'Success', 'data' => $rating], 200);
    }
}
