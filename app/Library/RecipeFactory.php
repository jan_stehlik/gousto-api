<?php

namespace App\Library;


class RecipeFactory
{
    public static function create($customAttributes = [])
    {
        $defaultAttributes = [
            'id' => random_int(1, 1000),
            'created_at' => '1970-01-01 00:00:00',
            'updated_at' => '1970-01-01 00:00:00',
            'box_type' => '',
            'title' => '',
            'slug' => '',
            'short_title' => '',
            'marketing_description' => '',
            'calories_kcal' => '',
            'protein_grams' => '',
            'fat_grams' => '',
            'carbs_grams' => '',
            'bulletpoint1' => '',
            'bulletpoint2' => '',
            'bulletpoint3' => '',
            'recipe_diet_type_id' => '',
            'season' => '',
            'base' => '',
            'protein_source' => '',
            'preparation_time_minutes' => '',
            'shelf_life_days' => '',
            'equipment_needed' => '',
            'origin_country' => '',
            'recipe_cuisine' => '',
            'in_your_box' => '',
            'gousto_reference' => '',
        ];

        return array_merge($defaultAttributes, $customAttributes);
    }
}