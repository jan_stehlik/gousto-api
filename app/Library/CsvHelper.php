<?php

namespace App\Library;


class CsvHelper
{
    public function toArray($filename)
    {
        if (!file_exists($filename)) {
            return [];
        }

        return array_map('str_getcsv', file($filename));
    }

    public function fillDataKeys($data)
    {
        $withKeys = [];
        if ($data) {
            $keys = $data[0];
            $rows = array_slice($data, 1);
            foreach ($rows as $rowKey => $rowValue) {
                foreach ($rowValue as $itemKey => $itemValue) {
                    $withKeys[$rowKey][$keys[$itemKey]] = $itemValue;
                }
            }
        }

        return $withKeys;
    }

    public function insert($newRecord, $records, $filename)
    {
        if (!empty($records)) {
            $records[] = array_values($newRecord);
        } else {
            $records[] = array_keys($newRecord);
            $records[] = array_values($newRecord);
        }

        $fp = fopen($filename, 'w');
        foreach ($records as $record) {
            fputcsv($fp, $record, ',');
        }

        fclose($fp);

        return $newRecord;
    }

    public function update($id, $newData, $records, $filename)
    {
        $columnNames = $records[0];
        $updatedRecords[] = $columnNames;
        $updated = null;
        foreach ($this->fillDataKeys($records) as $record) {
            if ($record['id'] == $id) {
                foreach ($newData as $key => $newValue) {
                    if ($key == 'id' || !in_array($key, $columnNames)) {
                        continue;
                    }
                    $record[$key] = $newValue;
                }
                $updated = $record;
            }
            $updatedRecords[] = array_values($record);
        }

        $fp = fopen($filename, 'w');
        foreach ($updatedRecords as $record) {
            fputcsv($fp, $record, ',');
        }

        fclose($fp);

        return $updated;
    }
}